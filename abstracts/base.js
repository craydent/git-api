/*/---------------------------------------------------------/*/
/*/ Craydent LLC git-api-v0.1.0                             /*/
/*/ Copyright 2011 (http://craydent.com/about)              /*/
/*/ Dual licensed under the MIT or GPL Version 2 licenses.  /*/
/*/ (http://craydent.com/license)                           /*/
/*/---------------------------------------------------------/*/
/*/---------------------------------------------------------/*/
var Abstract = require('noof').Abstract;
Abstract(function Base () {
	public.string.user;
	public.string.password;
	public.string.client_id;
	public.string.secret;
	public.string.token;

	public.construct = function (params) { _set_values(params); };
	protected.method._parseGURL = function (url) {
		var git_parts = url.split(':'),
			owner = (git_parts = git_parts[1].split('/'))[0],
			name = git_parts[1].split('.')[0];
		return { owner: owner, name: name };
	};
	public.method.createDeployKey = function (data) {};
	public.method.createWebhook = function (data) {};
	public.method.createDeployKey = function (url) {};
	public.method.createWebhook = function (url) {};
	protected.method._set_values = function (obj) {
		obj = obj || {};
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop) && self.hasOwnProperty(prop)) {
				self[prop] = obj[prop];
			}
		}
	};
});