/*/---------------------------------------------------------/*/
/*/ Craydent LLC git-api-v0.1.0                             /*/
/*/ Copyright 2011 (http://craydent.com/about)              /*/
/*/ Dual licensed under the MIT or GPL Version 2 licenses.  /*/
/*/ (http://craydent.com/license)                           /*/
/*/---------------------------------------------------------/*/
/*/---------------------------------------------------------/*/
require("../interfaces/client");
require("../abstracts/base");
var Public = require('noof').Public,
	Base = require('noof').Base,
	IClient = require('noof').IClient;

Public(function GitHub () {
	private.method._createDeployKey = function (data) {
		return $c.ajax({
			url: "https:\/\/api.github.com/repos/" + data.owner + "/" + data.name + "/keys",
			headers: {
				'Authorization': 'Basic ' + new Buffer(self.user + ":" + self.password).toString('base64'),
				'Content-Type': 'application/json',
				'User-Agent': data.owner + "-" + data.name
			},
			method: "POST",
			data: {"title": data.label, "key": data.key, "read_only": true}
		});
	};
	private.method._createWebhook = function (data) {
		return $c.ajax({
			url: "https:\/\/api.github.com/repos/" + data.owner + "/" + data.name + "/hooks",
			headers: {
				'Authorization': 'Basic ' + new Buffer(self.user + ":" + self.password).toString('base64'),
				'Content-Type': 'application/json',
				'User-Agent': data.owner + "-" + data.name
			},
			method: "POST",
			data: {
				name: "web",
				config: {url: data.url, content_type: "json"},
				events: data.events || ["push", "pull_request"],
				active: true
			}
		});
	};
	public.method.createDeployKey = function (data) {
		return _createDeployKey(data);
	};
	public.method.createDeployKey = function (url,label,key) {
		var obj = _parseGURL(url);
		obj.label = label;
		obj.key = key;

		return _createDeployKey(obj);
	};
	public.method.createWebhook = function (data) {
		return _createWebhook(data);
	};
	public.method.createWebhook = function (url,hook_url) {
		var obj = _parseGURL(url);
		obj.url = hook_url;

		return _createDeployKey(obj);
	};
	public.method.createWebhook = function (url,hook_url,events) {
		var obj = _parseGURL(url);
		obj.url = hook_url;
		obj.events = events;

		return _createDeployKey(obj);
	};


	protected.method._set_values = function (obj) {
		obj = obj || {};
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop) && self.hasOwnProperty(prop)) {
				self[prop] = obj[prop];
			}
		}
	};
}).extendsFrom(Base).implementsInterface(IClient);