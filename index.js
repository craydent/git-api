/*/---------------------------------------------------------/*/
/*/ Craydent LLC git-api-v0.1.0                             /*/
/*/ Copyright 2011 (http://craydent.com/about)              /*/
/*/ Dual licensed under the MIT or GPL Version 2 licenses.  /*/
/*/ (http://craydent.com/license)                           /*/
/*/---------------------------------------------------------/*/
/*/---------------------------------------------------------/*/
require('craydent/noConflict');
$c.requireDirectory('./clients','r');
function GAPI (options) {
	options = options || {};
	var self = this;
	this.user = options.user;
	this.password = options.password;
	this.client = options.client;
	this.secret = options.secret;
	this.token = options.token;

	this.createDeployKey = function (data) {
		var client = _getClient(data);
		if (client) {
			return (new client(this)).createDeployKey(data);
		}
	};
	this.createWebhook = function (data) {
		var client = _getClient(data);
		if (client) {
			return (new client(this)).createWebhook(data);
		}
	};;
	var _getClient = function (data) {
		var url = data.url;
		switch (true) {
			case $c.contains(url,'github.com'):
				return GitHub;
				break;
			case $c.contains(url,'bitbucket.com'):
				return GitHub;
		}
	};
}
module.exports = GAPI;
