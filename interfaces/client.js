/*/---------------------------------------------------------/*/
/*/ Craydent LLC git-api-v0.1.0                             /*/
/*/ Copyright 2011 (http://craydent.com/about)              /*/
/*/ Dual licensed under the MIT or GPL Version 2 licenses.  /*/
/*/ (http://craydent.com/license)                           /*/
/*/---------------------------------------------------------/*/
/*/---------------------------------------------------------/*/
var Interface = require('noof').Interface;
Interface(function IClient () {
	public.string.user;
	public.string.password;
	public.string.client_id;
	public.string.secret;
	public.string.token;

	public.method.createDeployKey(Object.data);
	//public.method.createDeployKey(String.url, String.label, String.key);
	public.method.createWebhook(Object.data);
	//public.method.createWebhook(String.url, String.hook_url);
	//public.method.createWebhook(String.url, String.hook_url, Array.events);
});