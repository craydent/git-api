/*/---------------------------------------------------------/*/
/*/ Craydent LLC git-api-v0.1.0                             /*/
/*/ Copyright 2011 (http://craydent.com/about)              /*/
/*/ Dual licensed under the MIT or GPL Version 2 licenses.  /*/
/*/ (http://craydent.com/license)                           /*/
/*/---------------------------------------------------------/*/
/*/---------------------------------------------------------/*/
require("../interfaces/client");
require("../abstracts/base");
var Public = require('noof').Public,
	Base = require('noof').Base,
	IClient = require('noof').IClient;

Public(function BitBucket () {
	private.method._createDeployKey = function (data) {
		return $c.ajax({
			url: "https:\/\/api.bitbucket.org/1.0/repositories/" + data.owner + "/" + data.name + "/deploy-keys",
			headers: {
				'Authorization': 'Basic ' + new Buffer(self.user + ":" + self.password).toString('base64'),
				'Content-Type': 'application/json',
				'User-Agent': data.owner + "-" + data.name
			},
			method: "POST",
			data: {"label": data.label, "key": data.key}
		});
	};
	private.method._createWebhook = function (data) {
		return $c.ajax({
			url: "https:\/\/api.bitbucket.org/2.0/repositories/" + data.owner + "/" + data.name + "/hooks",
			headers: {
				'Authorization': 'Basic ' + new Buffer(self.user + ":" + self.password).toString('base64'),
				'Content-Type': 'application/json',
				'User-Agent': data.owner + "-" + data.name
			},
			method: "POST",
			data: {
				description: data.description || data.name,
				url: data.url,
				events: data.events || ["repo:push", "pullrequest:fulfilled"],
				active: true
			}
		});
	};
	public.method.createDeployKey = function (data) {
		return _createDeployKey(data);
	};
	public.method.createDeployKey = function (url,label,key) {
		var obj = _parseGURL(url);
		obj.label = label;
		obj.key = key;

		return _createDeployKey(obj);
	};
	public.method.createWebhook = function (data) {
		return _createWebhook(data);
	};
	public.method.createWebhook = function (url,hook_url) {
		var obj = _parseGURL(url);
		obj.url = hook_url;

		return _createDeployKey(obj);
	};
	public.method.createWebhook = function (url,hook_url,events) {
		var obj = _parseGURL(url);
		obj.url = hook_url;
		obj.events = events;

		return _createDeployKey(obj);
	};

	protected.method._set_values = function (obj) {
		obj = obj || {};
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop) && self.hasOwnProperty(prop)) {
				self[prop] = obj[prop];
			}
		}
	};
}).extendsFrom(Base).implementsInterface(IClient);